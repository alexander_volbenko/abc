package vak.Math;

import org.junit.Test;
import vak.Math.exceptions.*;

import static org.junit.Assert.*;

public class MathIntegerExceptionTest {

    @Test(expected = OverflowException.class)
    public void sumOverflowException() throws OverflowException {
        int result = MathInteger.calcul("2147483647 + 1");
        assertEquals(Integer.MAX_VALUE + 1, result);
    }

    @Test(expected = OverflowException.class)
    public void subOverflowException() throws OverflowException {
        int result = MathInteger.calcul("-2147483647 - 2");
        assertEquals(Integer.MIN_VALUE - 2, result);
    }

    @Test(expected = OverflowException.class)
    public void multOverflowException() throws OverflowException {
        int result = MathInteger.calcul("2147483647 * 2");
        assertEquals(Integer.MAX_VALUE * 2, result);
    }

    @Test(expected = DivisionZeroException.class)
    public void divDivisionZeroException() throws DivisionZeroException {
        int result = MathInteger.calcul("2147483647 / 0");
        assertEquals(2147483647 / 0, result);
    }

    @Test(expected = OverflowException.class)
    public void powOverflowException() throws OverflowException {
        int result = MathInteger.calcul("2147483647 ^ 2");
        assertEquals((int) Math.pow(Integer.MAX_VALUE, 2), result);
    }

    @Test(expected = PowerNegativeException.class)
    public void powNegativeException() throws PowerNegativeException {
        int result = MathInteger.calcul("2147483647 ^ -2");
        assertEquals((int) Math.pow(2, -3), result);
    }

    @Test(expected = WrongExpression.class)
    public void wrongActExceptoin() throws WrongExpression {
        int result = MathInteger.calcul("2147483647& 1");
        assertEquals((int) Math.pow(2, -3), result);
    }
}