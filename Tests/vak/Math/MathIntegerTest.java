package vak.Math;


import org.junit.Test;

import static org.junit.Assert.*;

public class MathIntegerTest {

    @Test
    public void sumAboveZero() throws ArithmeticException {
        int result = MathInteger.calcul("40 + 20");
        assertEquals(40 + 20, result);
    }

    @Test
    public void sumLessThanZero() throws ArithmeticException {

        int result = MathInteger.calcul("-40 + -20");
        assertEquals(-40 + -20, result);
    }

    @Test
    public void sumOpposite() throws ArithmeticException {

        int result = MathInteger.calcul("-40 + 20");
        assertEquals(-40 + 20, result);
    }

    @Test
    public void subAboveZero() throws ArithmeticException {

        int result = MathInteger.calcul("40 - 20");
        assertEquals(40 - 20, result);
    }

    @Test
    public void subLessThanZero() throws ArithmeticException {

        int result = MathInteger.calcul("-40 - -20");
        assertEquals(-40 - -20, result);
    }

    @Test
    public void subOpposite() throws ArithmeticException {

        int result = MathInteger.calcul("-40 - 20");
        assertEquals(-40 - 20, result);
    }

    @Test
    public void multAboveZero() throws ArithmeticException {

        int result = MathInteger.calcul("40 * 20");
        assertEquals(40 * 20, result);
    }

    @Test
    public void multLessThanZero() throws ArithmeticException {

        int result = MathInteger.calcul("-40 * -20");
        assertEquals(-40 * -20, result);
    }

    @Test
    public void multOpposite() throws ArithmeticException {

        int result = MathInteger.calcul("-40 * 20");
        assertEquals(-40 * 20, result);
    }

    @Test
    public void divAboveZero() throws ArithmeticException {

        int result = MathInteger.calcul("40 / 20");
        assertEquals(40 / 20, result);
    }

    @Test
    public void divLessThanZero() throws ArithmeticException {

        int result = MathInteger.calcul("-40 / -20");
        assertEquals(-40 / -20, result);
    }

    @Test
    public void divOpposite() throws ArithmeticException {
        int result = MathInteger.calcul("-40 / 20");
        assertEquals(-40 / 20, result);
    }

    @Test
    public void powAboveZero() throws ArithmeticException {
        int result = MathInteger.calcul("13 ^ 3");
        assertEquals((int) Math.pow(13, 3), result);
    }
}