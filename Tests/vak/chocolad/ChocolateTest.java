package vak.chocolad;

import org.junit.Test;

import static org.junit.Assert.*;

public class ChocolateTest {

    @Test
    public void buy() {
            int money = 1 + (int) (Math.random() * 100);
            int result = Chocolate.buy(money);
            assertEquals(money+(money/3),result);

        }
}