package vak.chocolad;

import java.util.Scanner;

/**
 * Класс Chocolate выводит кол-во купленных шоколадок
 *
 * @author Волбенко А.К 17ИТ18
 */
public class Chocolate {
    public static void main(String[] args) {
        Scanner sc= new Scanner(System.in);
        int money = sc.nextInt();
        System.out.print(Chocolate.buy(money));
    }

    /**
     *
     * @param money - кол-во денег
     * @return - полученные шоколадки
     */
    public static int buy (int money){
        int wrap = money / 3;
        return money + wrap;
    }
}
