package vak.animalRace;

public class RabbitAndTurtle {

    public static void main(String[] args) {
        Thread rabbit = new AnimalThread("rabbit", 10);
        Thread turtle = new AnimalThread("turtle", 5);
        rabbit.start();
        turtle.start();
    }
}
