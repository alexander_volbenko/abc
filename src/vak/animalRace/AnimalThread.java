package vak.animalRace;

public class AnimalThread extends Thread {

    public AnimalThread(String name, int priority) {
        setName(name);
        setPriority(priority);
    }

    public synchronized void run() {
        for (int range = 0; range < 50; range++) {
            System.out.println(getName() + " " + range);
            if (getName() == "rabbit" && range == 37) {
                setPriority(3);
            }
        }

    }
}
