package vak.bankAccount;

/**
 * Класс, запускающий потоки снятие и вложения денег на аккаунт
 *
 * @author Волбенко А.К.
 */
public class main {

    public static void main(String[] args) {
        Account Account = new Account(20000);
        System.out.println(Account.checkBalance());
        PutMoneyThread putMoneyThread = new PutMoneyThread(Account, 10000);
        GetMoneyThread getMoneyThread = new GetMoneyThread(Account, 20000, putMoneyThread);
        putMoneyThread.start();
        getMoneyThread.start();
        try {
            putMoneyThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
