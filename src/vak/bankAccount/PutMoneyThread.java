package vak.bankAccount;

/**
 * Класс, реализующий поток зачисления денег
 *
 * @author Волбенко А.К
 */
public class PutMoneyThread extends Thread {
    private Account account;
    private long money;

    public PutMoneyThread(Account account, long money) {
        this.account = account;
        this.money = money;
    }

    /**
     * Метод потока, который зачисляет деньги на баланс
     */
    public synchronized void run() {
        for (int i = 0; i < 3; i++) {
            account.putMoney(money);
        }
    }
}