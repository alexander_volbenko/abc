package vak.bankAccount;

/**
 * Класс, реализиющий поток снятия денег
 */
public class GetMoneyThread extends Thread {
    private Account account;
    private long money;
    private PutMoneyThread putMoneyThread;

    public GetMoneyThread(Account account, long money, PutMoneyThread putMoneyThread) {
        this.account = account;
        this.money = money;
        this.putMoneyThread = putMoneyThread;
    }

    /**
     * Метод потока, который снимает деньги
     */
    public void run() {
        while (true) {
            if (putMoneyThread.getState() != State.TERMINATED || account.checkBalance() >= 20000) {
                account.getMoney(money, putMoneyThread);
            } else {
                System.exit(0);
            }
        }
    }
}
