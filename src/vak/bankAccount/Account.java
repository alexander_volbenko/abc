package vak.bankAccount;


import vak.bankAccount.exceptions.NegativeBalance;
import vak.bankAccount.exceptions.NegativeMoney;

/**
 * Класс Аккаунта и банальных действий в виде снятие и вложение денег
 *
 * @author Волбенко А.К
 */
public class Account {
    private long balance;
    private long balanceTest;

    public Account(long balance) {
        this.balance = balance;
    }

    /**
     * Проверка количества денег на отрицательное цисло
     *
     * @param money - количества денег исп. в действие
     * @throws NegativeMoney
     */
    private void checkMoneyOnNegative(long money) throws NegativeMoney {
        try {
            if (money < 0) {
                throw new NegativeMoney();
            }
        } catch (NegativeMoney E) {
            throw new NegativeMoney();
        }
    }

    /**
     * Проверка баланса на отрицательное значение
     *
     * @param balanceTest - тестовый баланс который проверяется, чтобы не трогать основную переменную
     * @return - отправляет результят проверки: false в положительном случае и ошибку в отрицательном
     * @throws NegativeBalance - ошибка говорящая о негативном балансе
     */
    private boolean checkBalanceOnNegative(long balanceTest) throws NegativeBalance {
        if (balanceTest < 0) {
            throw new NegativeBalance();
        } else {
            return false;
        }
    }

    /**
     * Метод позволяющий позволяющий зачислить деньги на баланс
     *
     * @param money - кол-во зачисляемых денег
     */
    public synchronized void putMoney(long money) {
        try {
            while (balance >= 20000) {
                wait();
            }
            balanceTest = balance;
            checkMoneyOnNegative(money);
            balanceTest += money;
            checkBalanceOnNegative(balanceTest);
            balance = balanceTest;
            System.out.println(checkBalance());
            notify();
        } catch (NegativeMoney E) {
            E.printStackTrace();
        } catch (NegativeBalance E) {
            E.printStackTrace();
        } catch (InterruptedException E) {
            E.printStackTrace();
        }
    }

    /**
     * Метод, позволяющий снять деньги
     *
     * @param money          - кол-во денег которое нужно снять
     * @param putMoneyThread - передача потока для знание его состояние
     */
    public synchronized void getMoney(long money, PutMoneyThread putMoneyThread) {
        try {
            while (balance < 20000 && putMoneyThread.getState() != Thread.State.TERMINATED) {
                wait(1);
            }
            if (putMoneyThread.getState() != Thread.State.TERMINATED) {
                balanceTest = balance;
                checkMoneyOnNegative(money);
                balanceTest -= money;
                checkBalanceOnNegative(balanceTest);
                balance = balanceTest;
                System.out.println(checkBalance());
                notify();
            }
        } catch (NegativeMoney E) {
            E.printStackTrace();
        } catch (NegativeBalance E) {
            E.printStackTrace();
        } catch (InterruptedException E) {
            E.printStackTrace();
        }

    }

    /**
     * Метод, который выводит состояние баланса
     *
     * @return - вывод баланса
     */
    public long checkBalance() {
        {
            return balance;
        }
    }
}
