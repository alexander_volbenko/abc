package vak.threads;

public class Threads {
    public static void main(String[] args) {
        System.out.println("Главный поток запущен");
        for (int i = 0; i < 10; i++) {
            NewThread thread = new NewThread();
            thread.start();
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Главный поток завершен");
    }
}
