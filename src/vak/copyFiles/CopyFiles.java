package vak.copyFiles;

import java.io.*;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

/**
 * Класс, копирующий файлы из папки Ctrl_C в папку Ctrl_V
 *
 * @author Волбенко А.К.
 */
public class CopyFiles {

    public static ArrayList names = new ArrayList();

    public static void main(String[] args) {
        File dir = new File("src\\vak\\copyFiles\\Ctrl_C");
        for (File item : dir.listFiles()) {
            names.add(item.getName());
        }
        for (int count = 0; count < names.size(); count++) {
            Thread threadCopy = new ThreadCopy(count);
            threadCopy.start();
        }
    }

    /**
     * Метод осуществляет копирование файлов
     * @param count - место в массиве для выбора файла
     */
    public static void copy(int count) {
        try {
            FileChannel sourceChannel = new FileInputStream("src\\vak\\copyFiles\\Ctrl_C\\" + names.get(count)).getChannel();
            FileChannel destChannel = new FileOutputStream("src\\vak\\copyFiles\\Ctrl_V\\" + names.get(count)).getChannel();
            destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
