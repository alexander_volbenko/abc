package vak.Math;

import vak.Math.exceptions.*;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Класс MathsFile записывает в output.txt ответы на примеры из input.txt
 *
 * @author Волбенко А.К 17ИТ18
 */
public class MathsFile {
    public static void main(String[] args) throws IOException{
        ArrayList<String> lines = reader();
        try (FileWriter fw = new FileWriter("src//vak//Math//Files//output.txt")) {
            for (int i = 0; i < lines.size(); i++) {
                try {
                    String expression = lines.get(i);
                    fw.write((MathInteger.calcul(expression)) + "\n");
                } catch (WrongExpression E) {
                    fw.write("Неверные выражение\n");
                } catch (OverflowException E) {
                    fw.write("Переполнение ответа\n");
                } catch (PowerNegativeException E) {
                    fw.write("Деление на отрицательное число\n");
                } catch (DivisionZeroException E) {
                    fw.write("Деление на ноль\n");
                }
            }
        }catch (IOException E){
            System.out.println("IOException");
        }
    }

    /**
     * Читает файл и вносит строки в ArrayList, который возвращается
     * @return - ArrayList с линиями файла
     * @throws IOException - ошика чтения
     */
    private static ArrayList<String> reader() throws IOException{
        ArrayList lines = new ArrayList<String>();
        try (FileReader fr = new FileReader("src//vak//Math//Files//input.txt")) {
            Scanner sc = new Scanner(fr);
            while (sc.hasNextLine()) {
                lines.add(sc.nextLine());
            }
        } catch (IOException E) {
            throw new IOException();
        }
        return lines;
    }
}
