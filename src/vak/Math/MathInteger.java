package vak.Math;


import vak.Math.exceptions.*;


/**
 * MathInteger - класс по типу калькулятора с вводом: n + n; действиями: +, -, *, /,^;
 */
public class MathInteger {

    /**
     * Вычисляет String пример и выводит int ответ
     *
     * @param expression - пример в виде String переменной
     * @return - ответ примера в виде int переменной
     * @throws ArithmeticException - ошибки
     */
    public static int calcul(String expression) throws ArithmeticException {
        int reply = 0;
        String[] sign = expression.split(" ");
            if (!expression.matches("[+-]?[0-9]+\\s[+-/*^]\\s[+-]?[0-9]+")) {
                throw new WrongExpression();
            }
            int[] numbersMass = numbers(expression);
            int firstMultiplier = numbersMass[0];
            int secondMultiplier = numbersMass[1];

            switch (sign[1]) {
                case "+":
                    reply = firstMultiplier + secondMultiplier;
                    if (((firstMultiplier ^ reply) & (secondMultiplier ^ reply)) < 0) {
                        throw new OverflowException();
                    } else {
                        return reply;
                    }
                case "-":
                    reply = firstMultiplier - secondMultiplier;

                    if (((firstMultiplier ^ secondMultiplier) & (firstMultiplier ^ reply)) < 0) {
                        throw new OverflowException();
                    } else {
                        return reply;
                    }
                case "*":
                    if (firstMultiplier < 0 && secondMultiplier < 0) {
                        return firstMultiplier * secondMultiplier;
                    }

                    reply = firstMultiplier * secondMultiplier;

                    if (((firstMultiplier ^ reply) & (secondMultiplier ^ reply)) < 0) {
                        throw new OverflowException();
                    } else {
                        return reply;
                    }
                case "/":
                    if (secondMultiplier == 0) {
                        throw new DivisionZeroException();
                    }

                    if (firstMultiplier < 0 && secondMultiplier < 0) {
                        return firstMultiplier / secondMultiplier;
                    }

                    reply = firstMultiplier / secondMultiplier;

                    if (((firstMultiplier ^ reply) & (secondMultiplier ^ reply)) < 0) {
                        throw new OverflowException();
                    } else {
                        return reply;
                    }
                case "^":
                    if (secondMultiplier == 0) {
                        return 1;
                    }

                    if (firstMultiplier == 0) {
                        return 0;
                    }

                    if (secondMultiplier < 0) {
                        throw new PowerNegativeException();
                    }

                    long outputLong = 1;
                    int outputTotal = 0;

                    for (int i = 0; i < secondMultiplier; i++) {
                        outputLong *= firstMultiplier;
                        if (outputLong >= Integer.MAX_VALUE) {
                            throw new OverflowException();
                        } else {
                            outputTotal = (int) outputLong;
                        }
                    }
                    return outputTotal;
            }

        return reply;
    }

    /**
     * Метод переводящий переменные примера в массив с 2 значениями int
     *
     * @param expression - String строка с примером
     * @return - возвращает int массив с 2 переменными для примера
     */
    private static int[] numbers(String expression) {
        int[] numbers = new int[2];
        try {
            String[] spliteLine = expression.split(" ");
            numbers[0] = Integer.parseInt(spliteLine[0]);
            numbers[1] = Integer.parseInt(spliteLine[2]);
        } catch (NumberFormatException E) {

        }
        return numbers;
    }
}