package vak.Math;

import vak.Math.exceptions.*;

import java.util.Scanner;

/**
 * Класс Maths выводит ответ на выражение
 *
 * @author Волбенко А.К 17ИТ18
 */
public class Maths {

    public static void main(String[] args) throws ArithmeticException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите выражение в формате: a + b ");
        String expression = sc.nextLine();
        try {
            System.out.println("Ответ: " + MathInteger.calcul(expression));
        } catch (WrongExpression E) {
            System.out.println("Неверные выражение");
         } catch (OverflowException E) {
            System.out.println("Переполнение ответа");
        } catch (PowerNegativeException E) {
            System.out.println("Деление на отрицательное число");
        } catch (DivisionZeroException E) {
            System.out.println("Деление на ноль");
        }

    }
}
