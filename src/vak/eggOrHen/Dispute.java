package vak.eggOrHen;

public class Dispute {

    public static void main(String[] args) {
        ThreadDespute egg = new ThreadDespute("Яйцо");
        ThreadDespute chicken = new ThreadDespute("Курица");
        egg.start();
        chicken.start();
        try {
            egg.join();
            if (chicken.isAlive()){
                Thread.sleep(5);
                System.out.println("Курица WIN");
            } else {
                Thread.sleep(5);
                System.out.println("Яйцо WIN");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
