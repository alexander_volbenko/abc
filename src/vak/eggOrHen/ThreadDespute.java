package vak.eggOrHen;

public class ThreadDespute extends Thread {

    ThreadDespute(String name) {
        setName(name);
        setPriority(1);
    }

    @Override
    public void run() {
        for (int i = 0; i < 3; i++) {
            System.out.println(getName());
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
